
Python HMC Library/Module
=========================

This is a basic library/module for IBM Hardware Management Console to control
Power Systems (AIX/Linux) logical partitions.

It was created by Kairo Araujo as study propose for Python and API. As I use
a lot IBM Power Systems I decide to use HMC REST API for that.

The current state of this project is **WORK IN PROGRESS**


Documentation
=============


phmc Module
-----------

Power HMC module (phmc)

```python
import phmc
```


Class HMC
---------

Class HMC('address', 'username', 'password')


```python
labhmc = phmc.HMC('192.168.0.100', 'hscroot', 'abc1234')
```


- Function login()

It creates the login session and returns the session itself.

```python
session = labhmc.login()
```

- Function hmc_info()

It returns the hmc info as a dictionary.

```python
labhmc.hmc_info()

{
    "hostname": "labhmc",
    "id": "5b8526fb-46dd-3e54-a3c2-d9af36172aa1",
    "network": {
        "eth0": {
            "ipv4": "192.168.0.1",
            "ipv6": "fe80:0:0:0:3640:b5ff:fea2:4c8"
        },
        "eth1": {
            "ipv4": "10.0.0.1",
            "ipv6": "fe80:0:0:0:3640:b5ff:fea2:4ca"
        },
        "eth3": {
            "ipv4": "172.16.100.249",
            "ipv6": "fe80:0:0:0:3640:b5ff:fea6:c9d2"
        }
    },
    "version": "V8R8.3.0"
}

```

Class Managed_System
--------------------

Class ManagedSystem('HMC object', session)

It uses the HMC object created with class HMC() and the session generated by
this class.

```python
systems = ManagedSystem(labhmc, session)
```

- Function list_all()

List all systems managed by HMC and basic informations.

```python
system.list_all()

{
    "LAB1": {
        "Available Memory": "1280",
        "Available Processor": "1",
        "Configurable Memory": "65536",
        "Configurable Processor": "4",
        "Installed Memory": "65536",
        "Installed Processor": "4",
        "Physical LED State": "true",
        "id": "001e9c75-a870-35cc-a44e-3e31a16468a6",
        "model": "E4C",
        "serial": "ZZZZZZZ",
        "type": "8202"
    },
    "LAB2": {
        "Available Memory": "4096",
        "Available Processor": "0.8",
        "Configurable Memory": "65536",
        "Configurable Processor": "4",
        "Installed Memory": "65536",
        "Installed Processor": "4",
        "Physical LED State": "true",
        "id": "812a8565-585b-341c-ad52-647d9da5b646",
        "model": "E4C",
        "serial": "XXXXXXX",
        "type": "8202"
    },
    "LAB3": {
        "Available Memory": "292864",
        "Available Processor": "15.4",
        "Configurable Memory": "524288",
        "Configurable Processor": "24",
        "Installed Memory": "524288",
        "Installed Processor": "24",
        "Physical LED State": "true",
        "id": "54f9df8b-39ef-366f-a275-4c62a9c2fab7",
        "model": "42A",
        "serial": "YYYYYYY",
        "type": "8286"
    }
}
```