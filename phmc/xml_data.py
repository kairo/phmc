#!/usr/bin/env python
# -*- coding: utf-8 -*-

def extract(data, item, text=True, position=0):
    if text:
        output =  data.find_all(item.lower())[position].text
    else:
        output = data.find_all(item.lower())

    return output