#!/usr/bin/env python
# -*- coding: utf-8 -*-

class RequestError(Exception):
    """ Exception for Request errors."""
    pass


class HTTPError(Exception):
    """ Exception for HTTP errors."""
    pass


class KeyError(Exception):
    """ Exception for Key errors."""
    pass

class WrongOption(Exception):
    """ Exception for Wrong Option errors."""
    pass
