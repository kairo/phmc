#!/usr/bin/env python
# -*- coding: utf-8 -*-


# Logon
#
def root_url(address):
    root_url = 'https://{0}:12443/'.format(address)

    return root_url

def logon_url(address):
    logon_url = '{0}rest/api/web/Logon'.format(address)
    return logon_url

def logon_header():
    logon_header = {'Content-Type': 'application/vnd.ibm.powervm.web+xml; '
                                    'type=LogonRequest'}
    return logon_header

def logon_data(username, password):
    logon_data = """<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                    <LogonRequest xmlns="http://www.ibm.com/xmlns/systems/power/firmware/web/mc/2012_10/" 
                    schemaVersion="V1_1_0">
                        <Metadata>
                            <Atom/>
                        </Metadata>
                        <UserID kb="CUR" kxe="false">{0}</UserID>
                        <Password kb="CUR" kxe="false">{1}</Password>
                    </LogonRequest>
                    """.format(username, password)
    return logon_data

def session_header(x_api_session):
    session_header = {'Content-Type': 'application/vnd.ibm.powervm.web+xml; '
                                    'type=LogonRequest',
                      'X-API-Session': x_api_session}
    return session_header

def job_header(x_api_session):
    job_header = {'Content-Type': 'application/vnd.ibm.powervm.web+xml; '
                                  'Type=JobRequest',
                      'X-API-Session': x_api_session}
    return job_header

def job_url(address, jobid):
    job_url = '{0}rest/api/uom/jobs/{1}'.format(address, jobid)

    return job_url

# Management Console (HMC)
#
def management_console_url(address):
    management_console_url = '{0}rest/api/uom/ManagementConsole'.format(
        address
    )
    return management_console_url

def management_console_header(x_api_session):
    management_console_header = {
        'Content-Type': 'application/vnd.ibm.powervm.web+xml; '
                        'type=LogonRequest',
        'X-API-Session': x_api_session}
    return management_console_header


# Managed System
def managed_system_url(address):
    managed_system_url = '{0}rest/api/uom/ManagedSystem'.format(address)
    return managed_system_url

def managed_system_header(x_api_session):
    managed_system_header = {
        'Content-Type': 'application/vnd.ibm.powervm.uom+xml; '
                        'Type=ManagedSystem',
        'X-API-Session': x_api_session}
    return managed_system_header

def managed_system_poweroff_url(address, systemid):
    managed_system_poweroff_url = (
        '{0}rest/api/uom/ManagedSystem/{1}/do/PowerOff'.format(
            address, systemid
        ))
    return managed_system_poweroff_url

def managed_system_poweroff_data(imediate=True, restart=True):
    managed_system_poweroff_data = """ 
    <JobRequest
        xmlns="http://www.ibm.com/xmlns/systems/power/firmware/web/mc/2012_10/"
        xmlns:ns2="http://www.w3.org/XML/1998/namespace/k2" schemaVersion="V1_0">
        <Metadata>
            <Atom/>
        </Metadata>
        <RequestedOperation kxe="false" kb="CUR" schemaVersion="V1_0">
            <Metadata>
                <Atom/>
            </Metadata>
            <OperationName kxe="false" kb="ROR">PowerOff</OperationName>
            <GroupName kxe="false" kb="ROR">ManagedSystem</GroupName>
            <ProgressType kxe="false" kb="ROR">LINEAR</ProgressType>
        </RequestedOperation>
        <JobParameters kb="CUR" kxe="false" schemaVersion="V1_0">
            <Metadata>
                <Atom/>
            </Metadata>
            <JobParameter schemaVersion="V1_0">
                <Metadata>
                    <Atom/>
                </Metadata>
                <ParameterName kb="ROR" kxe="false">immediate</ParameterName>
                <ParameterValue kxe="false" kb="CUR">true</ParameterValue>
            </JobParameter>
            <JobParameter schemaVersion="V1_0">
                <Metadata>
                    <Atom/>
                </Metadata>
                <ParameterName kb="ROR" kxe="false">restart</ParameterName>
                <ParameterValue kxe="false" kb="CUR">true</ParameterValue>
            </JobParameter>
        </JobParameters>
    </JobRequest>
    """.format(imediate, restart)

    return managed_system_poweroff_data

def managed_system_poweron_url(address, systemid):
    managed_system_poweron_url = (
        '{0}rest/api/uom/ManagedSystem/{1}/do/PowerOn'.format(
            address, systemid
        ))
    return managed_system_poweron_url

def managed_system_poweron_data(imediate=True, restart=True):
    managed_system_poweron_data = """ 
    <JobRequest
    xmlns="http://www.ibm.com/xmlns/systems/power/firmware/web/mc/2012_10/"
    xmlns:ns2="http://www.w3.org/XML/1998/namespace/k2" schemaVersion="V1_0">
        <Metadata>
            <Atom/>
        </Metadata>
        <RequestedOperation kxe="false" kb="CUR" schemaVersion="V1_0">
            <Metadata>
                <Atom/>
            </Metadata>
            <OperationName kxe="false" kb="ROR">PowerOn</OperationName>
            <GroupName kxe="false" kb="ROR">ManagedSystem</GroupName>
            <ProgressType kxe="false" kb="ROR">LINEAR</ProgressType>
        </RequestedOperation>
        <JobParameters kxe="false" kb="CUR" schemaVersion="V1_0">
            <Metadata>
                <Atom/>
            </Metadata>
            <JobParameter schemaVersion="V1_0">
                <Metadata>
                    <Atom/>
                </Metadata>
                <ParameterName kxe="false" kb="ROR">operation</ParameterName>
                <ParameterValue kxe="false" kb="CUR">on</ParameterValue>
            </JobParameter>
        </JobParameters>
    </JobRequest>  
    """

    return managed_system_poweron_data


# Logical Partition
def logical_partition_all_url(address):
    logical_partition_all_url = (
        '{0}rest/api/uom/LogicalPartition'.format(address))

    return logical_partition_all_url

def logical_partition_header(x_api_session):
    logical_partition_header = {
        'Content-Type': 'application/vnd.ibm.powervm.uom+xml; '
                        'Type=LogicalPartition',
        'X-API-Session': x_api_session}

    return logical_partition_header

def logical_partition_url(address, lpar_uuid):

    logical_partition_url = (
        '{0}rest/api/uom/LogicalPartition/{1}'.format(
            address, lpar_uuid
        ))

    return logical_partition_url

def logical_partition_poweroff_url(address, lpar_uuid):
    logical_partition_poweroff_url = (
        '{0}rest/api/uom/LogicalPartition/{1}/do/PowerOff'.format(address, lpar_uuid))

    return logical_partition_poweroff_url

def logical_partition_poweroff_data(
        immediate=False, restart=False, operation='shutdown'):
    logical_partition_poweroff_data = """
    <JobRequest
    xmlns="http://www.ibm.com/xmlns/systems/power/firmware/web/mc/2012_10/"
    xmlns:ns2="http://www.w3.org/XML/1998/namespace/k2" schemaVersion="V1_0">
        <Metadata>
            <Atom/>
        </Metadata>
        <RequestedOperation kxe="false" kb="CUR" schemaVersion="V1_0">
            <Metadata>
                <Atom/>
            </Metadata>
            <OperationName kxe="false" kb="ROR">PowerOff</OperationName>
            <GroupName kxe="false" kb="ROR">LogicalPartition</GroupName>
            <ProgressType kxe="false" kb="ROR">DISCRETE</ProgressType>
        </RequestedOperation>
        <JobParameters kxe="false" kb="CUR" schemaVersion="V1_0">
            <Metadata>
                <Atom/>
            </Metadata>
            <JobParameter schemaVersion="V1_0">
                <Metadata>
                    <Atom/>
                </Metadata>
                <ParameterName kxe="false" kb="ROR">immediate</ParameterName>
                <ParameterValue kxe="false" kb="CUR">{0}</ParameterValue>
            </JobParameter>
            <JobParameter schemaVersion="V1_0">
                <Metadata>
                    <Atom/>
                </Metadata>
                <ParameterName kxe="false" kb="ROR">restart</ParameterName>
                <ParameterValue kxe="false" kb="CUR">{1}</ParameterValue>
            </JobParameter>
            <JobParameter schemaVersion="V1_0">
                <Metadata>
                    <Atom/>
                </Metadata>
                <ParameterName kxe="false" kb="ROR">operation</ParameterName>
                <ParameterValue kxe="false" kb="CUR">{2}</ParameterValue>
            </JobParameter>
        </JobParameters>
    </JobRequest>  
    """.format(immediate, restart, operation)

    return logical_partition_poweroff_data

def logical_partition_poweron_url(address, lpar_uuid):
    logical_partition_poweroff_url = (
        '{0}rest/api/uom/LogicalPartition/{1}/do/PowerOn'.format(
            address, lpar_uuid))

    return logical_partition_poweroff_url

def logical_partition_poweron_data(bootmode, profile):
    logical_partition_poweron_data = """
    <JobRequest:JobRequest
    xmlns:JobRequest="http://www.ibm.com/xmlns/systems/power/firmware/web/mc/2012_10/"
    xmlns="http://www.ibm.com/xmlns/systems/power/firmware/web/mc/2012_10/"
    xmlns:ns2="http://www.w3.org/XML/1998/namespace/k2" schemaVersion="V1_0">
        <Metadata>
            <Atom/>
        </Metadata>
        <RequestedOperation kb="CUR" kxe="false" schemaVersion="V1_0">
            <Metadata>
                <Atom/>
            </Metadata>
            <OperationName kb="ROR" kxe="false">PowerOn</OperationName>
            <GroupName kb="ROR" kxe="false">LogicalPartition</GroupName>
        </RequestedOperation>
        <JobParameters kb="CUR" kxe="false" schemaVersion="V1_0">
            <Metadata>
                <Atom/>
            </Metadata>
            <JobParameter schemaVersion="V1_0">
                <Metadata>
                    <Atom/>
                </Metadata>
                <ParameterName kxe="false" kb="ROR">force</ParameterName>
                <ParameterValue kxe="false" kb="CUR">false</ParameterValue>
            </JobParameter>
            <JobParameter schemaVersion="V1_0">
                <Metadata>
                    <Atom/>
                </Metadata>
                <ParameterName kxe="false" kb="ROR">LogicalPartitionProfile</ParameterName>
                <ParameterValue kxe="false" kb="CUR">{1}</ParameterValue>
            </JobParameter>
            <JobParameter schemaVersion="V1_0">
                <Metadata>
                    <Atom/>
                </Metadata>
                <ParameterName kxe="false" kb="ROR">novsi</ParameterName>
                <ParameterValue kxe="false" kb="CUR">true</ParameterValue>
            </JobParameter>
            <JobParameter schemaVersion="V1_0">
                <Metadata>
                    <Atom/>
                </Metadata>
                <ParameterName kxe="false" kb="ROR">bootmode</ParameterName>
                <ParameterValue kxe="false" kb="CUR">{0}</ParameterValue>
                </JobParameter>
            <JobParameter schemaVersion="V1_0">
                <Metadata>
                    <Atom/>
                </Metadata>
                <ParameterName kxe="false" kb="ROR">keylock</ParameterName>
                <ParameterValue kxe="false" kb="CUR">manual</ParameterValue>
            </JobParameter>
    </JobParameters>
</JobRequest:JobRequest>
	
    """.format(bootmode, profile)

    return logical_partition_poweron_data