#!/usr/bin/env python
# -*- coding: utf-8 -*-
import phmc.exceptions

def http_responses(http_code):
    """
    Returns the HMC standard messages for HTTP codes.

    :param http_code: http code to be checked.
    :return: HTTP Message
    """

    try:
        http_status_code = {
            200: 'HTTP OK.',
            201: 'HTTP Created.',
            204: 'Logoff OK (HTTP No content.)',
            301: 'HTTP Moved permanently.',
            303: 'HTTP See other.',
            304: 'HTTP Not modified.',
            400: 'HTTP Bad request.',
            401: 'User/Password Invalid or Session invalid (HTTP Unauthorized.)',
            403: 'HTTP Forbidden.',
            404: 'HTTP Not found.',
            405: 'HTTP method not allowed web api.',
            500: 'HTTP Internal Server Error'
        }

        return http_status_code[http_code]

    except KeyError as e:
        phmc.exceptions.KeyError("HTTP Error {0} not defined.".format(e))