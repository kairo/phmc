#!/usr/bin/env python
# -*- coding: utf-8 -*-

import phmc.constants.hmc_api as hmc_api
import phmc.http_request as http_request
import phmc.xml_data as xml_data
import phmc.exceptions

class LogicalPartition():
    """ Logical Partition Class """

    def __init__(self, hmc_object, x_api_session):
        """
        The Logical Partition Class requires the HMC object (address) and a
        session.

        :param hmc_object: HMC object (address)
        :param x_api_session: session
        """
        self.address = str(hmc_object)
        self.x_api_session = x_api_session

    def list_all(self):
        """
        List Logical Partitions systems with information.

        :return: dictionary with Logical Partitions basic information.
        """
        data = http_request.get(
            hmc_api.logical_partition_all_url(self.address),
            hmc_api.logical_partition_header(self.x_api_session)
        )

        lpar_names = xml_data.extract(data, 'partitionname', text=False)
        lpar_uuids = xml_data.extract(data, 'partitionuuid', text=False)
        lpar_states = xml_data.extract(data, 'partitionstate', text=False)
        lpar_types = xml_data.extract(data, 'partitiontype', text=False)
        lpar_systems = str(xml_data.extract(
            data, 'associatedpartitionprofile', text=False)).split()[1].split(
            '/')[-1].replace("\"", '')
        lpar_os = xml_data.extract(data, 'operatingsystemversion', text=False)

        lpar_all = {}
        lpar_count = 0
        while lpar_count < len(lpar_names):
            lpar_all[lpar_names[lpar_count].text] = {
                'uuid': lpar_uuids[lpar_count].text,
                'state': lpar_states[lpar_count].text,
                'type': lpar_types[lpar_count].text,
                'system host': lpar_systems,
                'OS': lpar_os[lpar_count].text

            }

            lpar_count += 1

        return lpar_all

    def lpar_info(self, lpar_uuid):

        data = http_request.get(
            hmc_api.logical_partition_url(self.address, lpar_uuid),
            hmc_api.logical_partition_header(self.x_api_session)
        )

        print(data)

        profile_id = str(xml_data.extract(
            data, 'associatedpartitionprofile', text=False)).split()[1].split(
            '/')[-1].replace("\"", '')
        priority = xml_data.extract(data, 'availabilitypriority')
        proc_mode = xml_data.extract(data, 'currentprocessorcompatibilitymode')
        os = xml_data.extract(data, 'operatingsystemversion')
        partition_id = xml_data.extract(data, 'partitionid')
        max_vslots = xml_data.extract(data, 'maximumvirtualioslots')
        ame = xml_data.extract(data, 'activememoryexpansionenabled')
        ame_factor = xml_data.extract(data, 'expansionfactor')
        ams = xml_data.extract(data, 'activememorysharingenabled')
        mem_des = xml_data.extract(data, 'desiredmemory')
        mem_max = xml_data.extract(data, 'maximummemory')
        mem_min = xml_data.extract(data, 'minimummemory')
        cpu_des = xml_data.extract(data, 'desiredprocessingunits')
        cpu_max = xml_data.extract(data, 'maximumprocessingunits')
        cpu_min = xml_data.extract(data, 'minimumprocessingunits')
        vcpu_des = xml_data.extract(data, 'desiredvirtualprocessors')
        vcpu_max = xml_data.extract(data, 'maximumvirtualprocessors')
        vcpu_min = xml_data.extract(data, 'minimumvirtualprocessors')
        lpar_name = xml_data.extract(data, 'partitionname')
        system_host_id = str(
            xml_data.extract(
                data, 'associatedmanagedsystem',
                text=False)).split()[1].split(
                '/')[-1].replace("\"", '')
        migration_state = xml_data.extract(data, 'migrationstate')
        state = xml_data.extract(data, 'partitionstate')
        lpar_type = xml_data.extract(data, 'partitiontype')
        sharing_mode = xml_data.extract(data, 'sharingmode')
        uncapped_weight = xml_data.extract(data, 'uncappedweight')
        rmc_state = xml_data.extract(data, 'resourcemonitoringcontrolstate')
        rmc_ip = xml_data.extract(data, 'resourcemonitoringipaddress')

        lpar_info = {
            'Name': lpar_name,
            'Profile id': profile_id,
            'System Host id': system_host_id,
            'LPAR name': lpar_name,
            'Priority': priority,
            'Processor Type Mode': proc_mode,
            'Operation System': os,
            'Partition ID': partition_id,
            'Max Virtual Slots': max_vslots,
            'AME': ame,
            'AME Factor': ame_factor,
            'AMS': ams,
            'Memory Desired': mem_des,
            'Memory Maximum': mem_max,
            'Memory Minimum': mem_min,
            'CPU Desired': cpu_des,
            'CPU Maximum': cpu_max,
            'CPU Minimum': cpu_min,
            'vCPU Desired': vcpu_des,
            'vCPU Maximum': vcpu_max,
            'vCPU Minimum': vcpu_min,
            'Migration state:': migration_state,
            'State': state,
            'Type': lpar_type,
            'Sharing mode': sharing_mode,
            'Uncapped weight': uncapped_weight,
            'RMC state': rmc_state,
            'RMC IP': rmc_ip
        }

        return lpar_info

    def poweroff(
            self, lpar_uuid, immediate=False, restart=False, operation='shutdown'):

        operation_opts = ['shutdown', 'osshutdown', 'dumprestart', 'retrydump']
        if operation not in operation_opts:
            raise phmc.exceptions.WrongOption(
                "Invalid option for mode {0}".format(operation_opts))

        data = http_request.put(
            hmc_api.logical_partition_poweroff_url(self.address, lpar_uuid),
            hmc_api.logical_partition_poweroff_data(immediate, restart, operation),
            hmc_api.job_header(self.x_api_session)
        )

        job_info = {}
        jobid = xml_data.extract(data, 'jobid')
        jobstatus = xml_data.extract(data, 'status')

        job_info['id'] = jobid
        job_info['status'] = jobstatus

        return job_info

    def poweron(self, lpar_uuid, profile, boot_mode='norm'):
        boot_modes = ['norm', 'dd', 'of', 'sms']
        if boot_mode not in boot_modes:
            raise phmc.exceptions.WrongOption(
                "Invalid option for boot mode {0}".format(boot_modes))

        if not profile:
            raise phmc.exceptions.WrongOption(
                "Profile requires a valid profile name.")

        data = http_request.put(
            hmc_api.logical_partition_poweron_url(self.address, lpar_uuid),
            hmc_api.logical_partition_poweron_data(boot_mode, profile),
            hmc_api.job_header(self.x_api_session)
        )

        job_info = {}
        jobid = xml_data.extract(data, 'jobid')
        jobstatus = xml_data.extract(data, 'status')

        job_info['id'] = jobid
        job_info['status'] = jobstatus

        return job_info