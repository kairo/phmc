#!/usr/bin/env python
# -*- coding: utf-8 -*-

import phmc.constants.hmc_api as hmc_api
import phmc.http_request as http_request
import phmc.xml_data as xml_data

class Job():
    """ Managed System Class """
    def __init__(self, hmc_object, x_api_session):
        """
        The Managed System Class requires the HMC object (address) and a
        session.

        :param hmc_object: HMC object (address)
        :param x_api_session: session
        """
        self.address = str(hmc_object)
        self.x_api_session = x_api_session

    def status(self, jobid, debug=False):
        data = http_request.get(
            hmc_api.job_url(self.address, jobid),
            hmc_api.job_header(self.x_api_session)
        )

        operation_name_data = xml_data.extract(data, 'operationname')
        message_key_data = xml_data.extract(data, 'messagekey', text=False)
        message_data = xml_data.extract(data, 'parametervalue', text=False)

        job_status = {}
        job_status[jobid] = operation_name_data
        job_status['message_keys'] = []
        for key in message_key_data:
            job_status['message_keys'].append(key.text)

        job_status['message'] = message_data[-1].text

        if debug:
            return job_status, data
        else:
            return job_status



