#!/usr/bin/env python
# -*- coding: utf-8 -*-

import phmc.constants.hmc_api as hmc_api
import phmc.http_request as http_request
import phmc.xml_data as xml_data

class ManagedSystem():
    """ Managed System Class """
    def __init__(self, hmc_object, x_api_session):
        """
        The Managed System Class requires the HMC object (address) and a
        session.

        :param hmc_object: HMC object (address)
        :param x_api_session: session
        """
        self.address = str(hmc_object)
        self.x_api_session = x_api_session

    def list(self):
        """
        List the Managed systems with information.

        :return: dictionary with Managed System basic information.
        """
        data = http_request.get(
            hmc_api.managed_system_url(self.address),
            hmc_api.managed_system_header(self.x_api_session)
        )

        system_list = {}
        id_data = xml_data.extract(data, 'id', text=False)[1:]
        name_data = xml_data.extract(data, 'SystemName', text=False)
        machine_type_data = xml_data.extract(data, 'MachineType', text=False)
        machine_model_data = xml_data.extract(data, 'Model', text=False)
        machine_serial_data = xml_data.extract(data, 'SerialNumber', text=False)
        machine_inst_mem = xml_data.extract(
            data, 'InstalledSystemMemory', text=False)
        machine_conf_mem = xml_data.extract(
            data, 'ConfigurableSystemMemory', text=False)
        machine_avail_mem = xml_data.extract(
            data, 'CurrentAvailableSystemMemory', text=False)
        machine_inst_cpu = xml_data.extract(
            data, 'InstalledSystemProcessorUnits', text=False)
        machine_conf_cpu = xml_data.extract(
            data, 'ConfigurableSystemProcessorUnits', text=False)
        machine_avail_cpu = xml_data.extract(
            data, 'CurrentAvailableSystemProcessorUnits', text=False)
        machine_led_state = xml_data.extract(
            data, 'PhysicalSystemAttentionLEDState', text=False)

        count = 0
        while count < len(name_data):
            system_list[name_data[count].text] = {
                'id': id_data[count].text,
                'type': machine_type_data[count].text,
                'model': machine_model_data[count].text,
                'serial': machine_serial_data[count].text,
                'Installed Memory': machine_inst_mem[count].text,
                'Configurable Memory': machine_conf_mem[count].text,
                'Available Memory': machine_avail_mem[count].text,
                'Installed Processor': machine_inst_cpu[count].text,
                'Configurable Processor': machine_conf_cpu[count].text,
                'Available Processor': machine_avail_cpu[count].text,
                'Physical LED State': machine_led_state[count].text,
            }

            count += 1

        return system_list

    def power_off(self, systemid, imediate=True, restart=True):
        data = http_request.put(
            hmc_api.managed_system_poweroff_url(self.address, systemid),
            hmc_api.managed_system_poweroff_data(),
            hmc_api.job_header(self.x_api_session)
        )

        job_info = {}
        jobid = xml_data.extract(data, 'jobid')
        jobstatus = xml_data.extract(data, 'status')

        job_info['id'] = jobid
        job_info['status'] = jobstatus

        return job_info

    def power_on(self, systemid):
        data = http_request.put(
            hmc_api.managed_system_poweron_url(self.address, systemid),
            hmc_api.managed_system_poweron_data(),
            hmc_api.job_header(self.x_api_session)
        )

        job_info = {}
        jobid = xml_data.extract(data, 'jobid')
        jobstatus = xml_data.extract(data, 'status')

        job_info['id'] = jobid
        job_info['status'] = jobstatus

        return job_info