
from bs4 import BeautifulSoup
import requests
from phmc.constants.http_status import http_responses
import phmc.exceptions

def get(address, headers):
    """
    Execute GET request to HMC

    :param address: HMC url address
    :param headers: Headers
    :return: content
    """
    try:
        content = requests.get(address, headers=headers, verify=False)
        code = content.status_code
        output = BeautifulSoup(content.text, 'lxml')

        if code != 200:
            raise phmc.exceptions.HTTPError(http_responses(code), output)
        else:
            return output
    except requests.exceptions.ConnectionError as e:
        raise phmc.exceptions.RequestError(e)

def put(address, data, headers):
    """
    Execute PUT request to HMC

    :param address: HMC url address
    :param data: Data instructions
    :param headers: Headers
    :return: content
    """
    try:
        content = requests.put(address, data=data, headers=headers, verify=False)
        code = content.status_code
        output = BeautifulSoup(content.text, 'lxml')
        if code != 200:
            raise phmc.exceptions.HTTPError(http_responses(code), output)
        else:
            return output

    except requests.exceptions.ConnectionError as e:
            raise phmc.exceptions.RequestError(e)

def delete(address, headers):
    """
    Execute DELETE request to HMC

    :param address: HMC url address
    :param headers: Headers
    :return: contect
    """
    try:
        content = requests.delete(address, headers=headers, verify=False)
        code = content.status_code
        output = BeautifulSoup(content.text, 'lxml')

        if code != 204:
            raise phmc.exceptions.HTTPError(http_responses(code), output)
        else:
            return output
    except requests.exceptions.ConnectionError as e:
        raise phmc.exceptions.RequestError(e)
