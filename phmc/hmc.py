#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from phmc.constants.http_status import http_responses
import phmc.constants.hmc_api as hmc_api
import phmc.exceptions
import phmc.http_request
import requests
import requests.packages.urllib3

class HMC():
    """ HMC Class, to login, get information and logoff.
    """
    def __init__(
            self, address=None, username='hscroot', password='abc123',
            disable_warning=True):
        """
        Init a HMC class.

        :param address: HMC IP/hostname address
        :param username: username
        :param password: password
        :param disable_warning: boolean to manage warnings
        """

        self.address = hmc_api.root_url(address)
        self.logon_url = hmc_api.logon_url(self.address)
        self.header = hmc_api.logon_header()
        self.data = hmc_api.logon_data(username, password)

        if disable_warning:
            requests.packages.urllib3.disable_warnings()

    def __str__(self):
        """
        The object HMC will be his address itself.

        :return: HMC address
        """
        return self.address

    def login(self):
        """
        Login to HMC and generate the session.

        :return: x_api_session to be used as session
        """
        data = phmc.http_request.put(self.logon_url, self.data, self.header)
        x_api_session = data.find_all('x-api-session')[0].text

        return x_api_session

    def logoff(self, x_api_session=None):
        """
        Logoff from HMC

        :param x_api_session: the session received from login
        :return: message about the logoff
        """
        session = phmc.http_request.delete(
            self.logon_url, hmc_api.session_header(x_api_session))
        code = session.status_code
        msg = http_responses(code)

        return msg

    def hmc_info(self, x_api_session=None):
        """
        Get HMC information.

        :param x_api_session: session received from login
        :return: HMC information as dictionary
        """
        data = phmc.http_request.get(
            hmc_api.management_console_url(self.address),
            hmc_api.management_console_header(x_api_session)
        )

        hmc_info = {}

        hostname_data = data.find_all('managementconsolename')
        hmc_info['hostname'] = hostname_data[0].text.replace('\n','')

        version_data = data.find_all('baseversion')
        hmc_info['version'] = version_data[0].text.replace('\n', '')

        id_data = data.find_all('id')
        hmc_info['id'] = id_data[0].text.replace('\n', '')

        network_data = data.find_all('managementconsolenetworkinterface')
        network_list = []
        for network in network_data:
            network_list.append(
                list(filter(None, network.text.splitlines())))

        hmc_info['network'] = {}
        for net in network_list:
            adapter = net[0]
            net_ipv4 = net[1].split()[0]
            net_ipv6 = net[1].split()[1]
            hmc_info['network'].update(
                {adapter: {'ipv4': net_ipv4, 'ipv6': net_ipv6}})

        return hmc_info
