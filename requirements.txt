beautifulsoup4>=4.6.0
chardet>=3.0.4
lxml>=4.1.1
requests>=2.18.4
urllib3>=1.22
